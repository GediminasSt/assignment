package com.factris.assignment.service.contract;

import com.factris.assignment.model.Contract;
import com.factris.assignment.repository.ContractRepo;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

//Using simple Junit and mockito, since SpringRUnner is slow and requires lot of mocks
//This checks simple written functionality
public class ContractServiceImplTest {

    public static final String TEST_CONTRACT_NO = "ContractNO1";
    private final ContractServiceImpl contractService = new ContractServiceImpl(Mockito.mock(ContractRepo.class));

    private List<Contract> createTestContracts() {
        return List.of(
                //JAN Contract
                Contract.builder()
                        .id(1)
                        .active(true)
                        .additionalFee(11.11)
                        .startDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime())
                        .endDate(new GregorianCalendar(2020, Calendar.JANUARY, 30).getTime())
                        .contractNumber(TEST_CONTRACT_NO)
                        .daysIncluded(14)
                        .fixedFeeAmount(15.15).build(),
                //FEB Contract
                Contract.builder()
                        .id(2)
                        .active(true)
                        .additionalFee(22.22)
                        .startDate(new GregorianCalendar(2020, Calendar.FEBRUARY, 1).getTime())
                        .endDate(new GregorianCalendar(2020, Calendar.FEBRUARY, 30).getTime())
                        .contractNumber(TEST_CONTRACT_NO)
                        .daysIncluded(14)
                        .fixedFeeAmount(15.15).build(),
                //JAN contract without end date, used to attempted break functionality and update record without date
                Contract.builder()
                        .id(3)
                        .active(true)
                        .additionalFee(22.22)
                        .startDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime())
                        .endDate(null)
                        .contractNumber(TEST_CONTRACT_NO)
                        .daysIncluded(14)
                        .fixedFeeAmount(15.15).build()

        );
    }

    @Test
    public void shouldSaveNonOverlappingContracts() {
        Contract contractToSave =
                Contract.builder()
                        .id(null)
                        .active(true)
                        .additionalFee(11.11)
                        .startDate(new GregorianCalendar(2020, Calendar.MAY, 1).getTime())
                        .endDate(new GregorianCalendar(2020, Calendar.MAY, 20).getTime())
                        .contractNumber(TEST_CONTRACT_NO)
                        .daysIncluded(14)
                        .fixedFeeAmount(15.15).build();
        var testContractList = createTestContracts();

        var savedEntity = contractService.checkAgainstOverlappingContracts(contractToSave, testContractList);
        Assert.assertNull(savedEntity.getId()); // id should be not equal to existing entity ie 1 it should be null
        Assert.assertEquals(savedEntity, contractToSave);
    }

    @Test
    public void shouldUpdateLatestOverlappingContract() {
        var contractToSave =
                Contract.builder()
                        .id(null)
                        .active(true)
                        .additionalFee(44.44)
                        .startDate(new GregorianCalendar(2020, Calendar.JANUARY, 15).getTime())
                        .endDate(new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime())
                        .contractNumber(TEST_CONTRACT_NO)
                        .daysIncluded(14)
                        .fixedFeeAmount(15.15).build();
        var testContractList = createTestContracts();
        var savedEntity = contractService.checkAgainstOverlappingContracts(contractToSave, testContractList);
        Assert.assertEquals(savedEntity.getId(), testContractList.get(1).getId()); // id should be equal to exiting latest contract
        Assert.assertEquals(savedEntity, contractToSave);

    }

    @Test
    public void shouldSaveContractWithoutEndDate() {
        var contractToSave =
                Contract.builder()
                        .id(null)
                        .active(true)
                        .additionalFee(44.44)
                        .startDate(new GregorianCalendar(2020, Calendar.JANUARY, 15).getTime())
                        .endDate(null)
                        .contractNumber(TEST_CONTRACT_NO)
                        .daysIncluded(14)
                        .fixedFeeAmount(15.15).build();
        var testContractList = createTestContracts();
        var savedEntity = contractService.checkAgainstOverlappingContracts(contractToSave, testContractList);
        Assert.assertNull(savedEntity.getId());
        Assert.assertEquals(savedEntity, contractToSave);

    }

}
