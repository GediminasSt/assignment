package com.factris.assignment.service.contract;

import com.factris.assignment.model.Contract;

public interface ContractService {
    Contract createOrUpdate(Contract requestContract);
}
