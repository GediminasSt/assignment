package com.factris.assignment.service.contract;

import com.factris.assignment.model.Contract;
import com.factris.assignment.repository.ContractRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ContractServiceImpl implements ContractService {

    private final ContractRepo contractRepo;

    @Override
    public Contract createOrUpdate(Contract requestContract) {
        var existingContracts = contractRepo
                .findByContractNumber(requestContract.getContractNumber());
        return contractRepo.save(checkAgainstOverlappingContracts(requestContract, existingContracts));
    }

    protected Contract checkAgainstOverlappingContracts(Contract requestContract,
                                                        List<Contract> existingContracts) {

        //Im assuming that both contracts should have both dates in order to overlap each other
        //I had idea of resolving contracts end date if its null, by checking next contracts start date,
        //That way even contracts with empty end date would have period, but left it out since start date can be in the past
        if (Optional.ofNullable(requestContract.getEndDate()).isPresent())
            existingContracts
                    .stream()
                    .filter(contractEntity -> Optional.ofNullable(contractEntity.getEndDate()).isPresent())
                    .filter(contractEntity ->
                            !requestContract.getStartDate().after(contractEntity.getEndDate()) &&
                                    !contractEntity.getStartDate().after(requestContract.getEndDate()))
                    .max(Comparator.comparing(Contract::getId))
                    //would be good if i could avoid mutability here
                    .ifPresent(existingContract -> requestContract.setId(existingContract.getId()));
        return requestContract;
    }

}
