package com.factris.assignment.service.invoice;

import com.factris.assignment.payload.CalculateInvoiceFeePayload;

import java.math.BigDecimal;

public interface InvoiceFeeCalculatorService {
    BigDecimal calculateInvoiceFee(CalculateInvoiceFeePayload calculateInvoiceFeePayload);
}
