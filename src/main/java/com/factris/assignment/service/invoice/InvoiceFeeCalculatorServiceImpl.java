package com.factris.assignment.service.invoice;

import com.factris.assignment.model.Contract;
import com.factris.assignment.payload.CalculateInvoiceFeePayload;
import com.factris.assignment.repository.ContractRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@AllArgsConstructor
public class InvoiceFeeCalculatorServiceImpl implements InvoiceFeeCalculatorService{

    private final ContractRepo contractRepo;

    @Override
    public BigDecimal calculateInvoiceFee(CalculateInvoiceFeePayload calculateInvoiceFeePayload) {
        var contracts = contractRepo.findByContractNumber(calculateInvoiceFeePayload.getContractNumber());

        return contracts.stream()
                .filter(contract -> Optional.ofNullable(contract.getEndDate()).isPresent())
                .filter(contract -> calculateInvoiceFeePayload.getIssueDate().after(contract.getStartDate()) &&
                        calculateInvoiceFeePayload.getIssueDate().before(contract.getEndDate()))
                .max(Comparator.comparing(Contract::getId))
                .map(contractInPeriod -> calculate(contractInPeriod, calculateInvoiceFeePayload))
                //Should implement telling why its happening (in case of no contract for period etc)
                .orElseThrow(RuntimeException::new);
    }

    private static BigDecimal calculate(Contract contractInPeriod, CalculateInvoiceFeePayload calculateInvoiceFeePayload) {
        var fixedPeriodEndDate = dateAfterDays(calculateInvoiceFeePayload.getPurchaseDate(),
                contractInPeriod.getDaysIncluded());
        var daysOverdue = daysBetweenDates(fixedPeriodEndDate,
                Optional.ofNullable(calculateInvoiceFeePayload.getPaidDate()).orElse(new Date()));

        var invoiceAmount = calculateInvoiceFeePayload.getInvoiceAmount();
        return fee(contractInPeriod.getFixedFeeAmount(), invoiceAmount)
                .add(additionalFee(daysOverdue, contractInPeriod.getAdditionalFee(), invoiceAmount));
    }

    private static BigDecimal additionalFee(Long daysOverdue, Double percent, BigDecimal amount){
        return BigDecimal.valueOf(daysOverdue).multiply(fee(percent, amount));
    }

    private static BigDecimal fee(Double percent, BigDecimal amount) {
        return amount.multiply(BigDecimal.valueOf(percent/100));
    }

    private static Long daysBetweenDates(Date startDate, Date endDate) {
        return DAYS.between(startDate.toInstant(), endDate.toInstant());
    }

    private static Date dateAfterDays(Date date, Integer numberOfDays) {
        //-1 since its inclusive
        return new Date(date.toInstant().plus(numberOfDays-1, DAYS).toEpochMilli());
    }
}
