package com.factris.assignment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Persistable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private String contractNumber;

    @Column
    @NotNull
    private Boolean active;

    //dated dont deserve its sepparate table
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date startDate;

    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column
    @NotNull
    private Double fixedFeeAmount;

    @Column
    @NotNull
    @PositiveOrZero
    private Integer daysIncluded;

    @Column
    @NotNull
    private Double additionalFee;

}
