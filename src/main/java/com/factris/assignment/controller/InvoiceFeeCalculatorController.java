package com.factris.assignment.controller;

import com.factris.assignment.payload.CalculateInvoiceFeePayload;
import com.factris.assignment.service.invoice.InvoiceFeeCalculatorService;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;

@RestController
@RequestMapping("/")
@AllArgsConstructor
public class InvoiceFeeCalculatorController {

    private final InvoiceFeeCalculatorService invoiceFeeCalculatorService;

    @PostMapping("calculate_invoice_fee")
    @Transactional
    public BigDecimal calculateInvoiceFee(@Valid @RequestBody CalculateInvoiceFeePayload calculateInvoiceFeePayload){
        return invoiceFeeCalculatorService.calculateInvoiceFee(calculateInvoiceFeePayload);
    }
}
