package com.factris.assignment.controller;

import com.factris.assignment.model.Contract;
import com.factris.assignment.payload.ContractPayload;
import com.factris.assignment.service.contract.ContractService;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/")
@AllArgsConstructor
public class ManageContractController {

    private final ContractService contractService;

    @PostMapping("/create_contract")
    @Transactional
    //should not return entity but for free code its ok
    public Contract createContract(@Valid @RequestBody ContractPayload contractPayload){
        //I could have just ask for entity as payload, but i think more validation layers > less layers
        //Also return type should be made as "OK", saved, or ContractPayload
        return contractService.createOrUpdate(contractPayload.toEntity());
    }
}
