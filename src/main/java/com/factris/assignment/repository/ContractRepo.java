package com.factris.assignment.repository;

import com.factris.assignment.model.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepo extends JpaRepository<Contract, String> {
    List<Contract> findByContractNumber(String contractNumber);
}
