package com.factris.assignment.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalculateInvoiceFeePayload {

    @NotNull(message = "contract number empty")
    private String contractNumber;
    @NotNull(message = "issue date empty")
    private Date issueDate;
    @NotNull(message = "due date empty")
    private Date dueDate;

    private Date paidDate;
    @NotNull(message = "purchase date empty")
    private Date purchaseDate;

    //should be some Money class which would include currency code also
    @NotNull(message = "invoice amount empty")
    private BigDecimal invoiceAmount;

}
