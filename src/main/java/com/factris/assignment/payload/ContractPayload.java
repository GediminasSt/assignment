package com.factris.assignment.payload;

import com.factris.assignment.model.Contract;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractPayload {
    //in real world scenario i would probably make everything as string/var since
    //client might want include such symbols as % at the end which would be stripped,
    //boolean as [true, false, 0, 1, yes, no] for data consistency and making API more universal
    //And also thought about using Optional values, but that came after i wrote this class
    //Also not using primitives. Its not 1998 anymore
    @NotNull(message = "contract number empty")
    private String contractNumber;
    @NotNull(message = "active field empty")
    private Boolean active;
    @NotNull(message = "contract period empty")
    private ContractPeriod contractPeriod;
    @NotNull(message = "fixed fee amount empty")
    private Double fixedFeeAmount;
    @NotNull(message = "days included empty")
    @PositiveOrZero(message = "days included must be possitive or zero")
    private Integer daysIncluded;
    @NotNull(message = "additional fee required")
    private Double additionalFee;

    //I've had created interface for this method since i thought invoice calculation would require this method also
    //but deleted it
    public Contract toEntity() {
        return Contract
                .builder()
                .active(active)
                .additionalFee(additionalFee)
                .contractNumber(contractNumber)
                .daysIncluded(daysIncluded)
                .startDate(contractPeriod.getStartDate())
                .endDate(contractPeriod.getEndDate())
                .fixedFeeAmount(fixedFeeAmount)
                .build();
    }

    @Data
    @AllArgsConstructor
    public static class ContractPeriod {
        //Would implement additional optional payload field which would tell
        //what kind of date format it is, also make dates as string for that
        @NotNull(message = "start date not set")
        private Date startDate;
        private Date endDate;
    }
}
