Gediminas Staponas
Factris Java assignment

Application is not dockerized.
What i didnt include: 
1. Exception/Error handling
1. propper logging
1. spring bean separation via config from DEV to PROD etc,
1. propper locale of time
1. tests for InvoiceFeeCalculation service, integration tests


1. ```mvn clean install```
1. ```docker-compose up```
1. ```java -jar target/assignment-0.0.1-SNAPSHOT.jar```
1. import sql from sql/initial_contract_data ```mysql -u username -p root assignmentdb < initial_contract_data.sql```
1. access ```http://localhost:8080/swagger-ui-factris.html```
1. test createcontract with payload ```{
                                         "contractNumber": "A10",
                                         "active": true,
                                         "contractPeriod": {
                                           "startDate": "2019-09-01",
                                           "endDate": "2020-01-31"
                                         },
                                         "fixedFeeAmount": 1.9,
                                         "daysIncluded": 14,
                                         "additionalFee": 0.1
                                       }```
1. test calculatefee with payload ```{
                                       "contractNumber": "A10",
                                       "issueDate": "2019-01-01",
                                       "dueDate": "2019-01-15",
                                       "paidDate": "2019-01-20",
                                       "purchaseDate": "2019-01-05",
                                       "invoiceAmount": 1000.00
                                     }```
                                     
Retrospect

1. task was really fun
1. i wish i had more free time to complete invoice fee service test
1. i prioritized tasks and features in that order which i would want to see implemented first 
(working product, ease of running and checking, tests)
