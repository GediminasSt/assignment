INSERT INTO contract (id, active, additional_fee, contract_number, days_included, end_date, fixed_fee_amount, start_date)
VALUES
       (id, true, 0.1, 'A10', 14, '2019-09-30', 2, '2019-01-01'),
       (id, true, 0.1, 'A10', 14, null, 1.75, '2020-01-01'),
       (id, true, 0.1, 'B21', 14, Null, 2.1, '2020-01-01')